//! A silly implementation of a double linked list.

/// A node of this linked list
#[derive(Debug)]
struct Node {
    prev: Option<*mut Node>,
    next: Option<*mut Node>,
    value: i32,
}

impl Node {
    fn new(value: i32) -> Node {
        Node {
            prev: None,
            next: None,
            value: value,
        }
    }
}

/// The linked list struct, holding pointers to the head and tail. Inserting
/// in the front/back/middle takes O(1).
#[derive(Debug)]
pub struct List {
    head: Option<*mut Node>,
    tail: Option<*mut Node>,
    len: usize,
}

impl List {
    /// Push an item to the front of the list in O(1) time
    pub fn push_front(&mut self, value: i32) {
        let mut new_node = Node::new(value);

        match self.head {
            Some(prev_head) => {
                new_node.next = Some(prev_head);

                let boxed_node = Box::new(new_node);
                let ptr = Box::into_raw(boxed_node);

                unsafe {
                    (*prev_head).prev = Some(ptr);
                }

                self.head = Some(ptr);
                self.len += 1;
            },
            None => {
                let boxed_node = Box::new(new_node);
                let ptr = Box::into_raw(boxed_node);

                self.head = Some(ptr);
                self.tail = Some(ptr);
                self.len = 1;
            }
        }
    }

    /// Push an item to the back of the list in O(1) time
    pub fn push_back(&mut self, value: i32) {
        let mut new_node = Node::new(value);

        match self.tail {
            Some(prev_tail) => {
                new_node.prev = Some(prev_tail);

                let boxed_node = Box::new(new_node);
                let ptr = Box::into_raw(boxed_node);

                unsafe {
                    (*prev_tail).next = Some(ptr);
                }

                self.tail = Some(ptr);
                self.len += 1;
            },
            None => {
                let boxed_node = Box::new(new_node);
                let ptr = Box::into_raw(boxed_node);

                self.head = Some(ptr);
                self.tail = Some(ptr);
                self.len = 1;
            }
        }
    }

    /// Pop an item from the front in O(1) time
    pub fn pop_front(&mut self) -> Option<i32> {
        self.head.take().map(|old_head| {
            let value;

            unsafe {
                self.head = (*old_head).next;

                value = (*old_head).value;

                // the following line cleans the node's memory
                Box::from_raw(old_head);
            }

            if self.len == 1 {
                self.tail = None;
            }

            self.len -= 1;

            value
        })
    }

    /// Pop an item from the back in O(1) time
    pub fn pop_back(&mut self) -> Option<i32> {
        self.tail.take().map(|old_tail| {
            let value;

            unsafe {
                self.tail = (*old_tail).prev;

                value = (*old_tail).value;

                // the following line cleans the node's memory
                Box::from_raw(old_tail);
            }

            if self.len == 1 {
                self.head = None;
            }

            self.len -= 1;

            value
        })
    }

    /// Create an index pointing to a node of this list in O(n) time. This
    /// index allows for insertion/deletion in the middle and can be moved
    /// after being created. Since it holds a mutable reference to the list
    /// only one index can exist at a time.
    pub fn index(&mut self, index: usize) -> Option<ListIndex> {
        match index < self.len {
            false => None,
            true => {
                let mut cur_node = self.head.unwrap();

                for _i in 1..=index {
                    unsafe {
                        cur_node = (*cur_node).next.unwrap();
                    }
                }

                Some(ListIndex {
                    list: self,
                    ptr: cur_node,
                    index: index,
                })
            }
        }
    }

    /// Returns the length of this list in O(1) time.
    pub fn len(&self) -> usize {
        self.len
    }

    /// Creates a new empty list.
    pub fn new() -> List {
        List {
            head: None,
            tail: None,
            len: 0,
        }
    }
}

impl Drop for List {
    fn drop(&mut self) {
        while self.head.is_some() {
            let ptr = self.head.unwrap();

            unsafe {
                self.head = (*ptr).next;

                // deletes the node
                Box::from_raw(ptr);
            }
        }
    }
}

/// An index pointing to a node of a list. Useful for insertion/deletion
/// in the middle of the list in O(1) time (Although creating the index itself
/// is O(n)).
#[derive(Debug)]
pub struct ListIndex<'a> {
    list: &'a mut List,
    ptr: *mut Node,
    index: usize,
}

impl<'a> ListIndex<'a> {
    /// Returns the current position in the list to which this index is
    /// pointing.
    pub fn pos(&self) -> usize {
        self.index
    }

    /// Returns the value at the current index.
    pub fn value(&self) -> i32 {
        unsafe {
            (*self.ptr).value
        }
    }

    /// Inserts a new element to this list at the current index in O(1). The 
    /// element previously pointed by the index moves to the right so the index
    /// stays at the same position
    pub fn push(&mut self, value: i32) {
        let mut new_node = Node::new(value);

        match unsafe { (*self.ptr).prev } {
            // We are somewhere in the middle
            Some(prev_node) => {
                // new node points to its `prev` and `next`
                new_node.prev = Some(prev_node);
                new_node.next = Some(self.ptr);

                let boxed = Box::new(new_node);
                let new_ptr = Box::into_raw(boxed);

                unsafe {
                    // prev node points to new node
                    (*prev_node).next = Some(new_ptr);

                    // current index points back to the new node
                    (*self.ptr).prev = Some(new_ptr);
                }

                // this index now points to the newly created node
                self.ptr = new_ptr;
            },

            // wer're at the first position of the list
            None => {
                // new node points to the next (frist) node in the list
                new_node.next = Some(self.ptr);

                let boxed = Box::new(new_node);
                let new_ptr = Box::into_raw(boxed);

                // list head points to new node
                self.list.head = Some(new_ptr);

                unsafe {
                    // current index points back to the new node
                    (*self.ptr).prev = Some(new_ptr);
                }

                // this index now points to the newly created node
                self.ptr = new_ptr;
            }
        }

        // increase list size
        self.list.len += 1;
    }

    /// Replaces the value at the current index with the given one, returning
    /// the previous one.
    pub fn replace(&mut self, value: i32) -> i32 {
        let old_value;

        unsafe {
            old_value = (*self.ptr).value;

            (*self.ptr).value = value;
        }

        old_value
    }

    /// Removes the current node from the list moving the items with greater
    /// indexes one position to the left. O(1) time. Returns an index pointing
    /// to the element to the right of the removed one. If this was the
    /// rightmost element of the list returns None.
    pub fn pop(mut self) -> (i32, Option<ListIndex<'a>>) {
        self.list.len -= 1;

        match unsafe { (*self.ptr).next } {
            // There are more elements to the right, return a new index in the
            // same position pointing to the new element at it
            Some(next_node) => {
                let value;

                unsafe {
                    value =  (*self.ptr).value;

                    // make next node point to the previous one
                    (*next_node).prev = (*self.ptr).prev;

                    match (*self.ptr).prev {
                        // There's a previous node, adjust
                        Some(prev_node) => {
                            (*prev_node).next = (*self.ptr).next;
                        },

                        // There's no previous node, adjust list
                        None => {
                            self.list.head = Some(next_node);
                        }
                    }

                    // current node gets disposed
                    Box::from_raw(self.ptr);
                }

                self.ptr = next_node;

                (value, Some(self))
            },

            // Since there are no more elements to the right this index expires
            // and returns None
            None => {
                let value;

                unsafe {
                    value =  (*self.ptr).value;

                    match (*self.ptr).prev {
                        // There's a previous node, adjust
                        Some(prev_node) => {
                            (*prev_node).next = None;
                            self.list.tail = Some(prev_node);
                        },

                        // There's no previous node, adjust list
                        None => {
                            self.list.head = None;
                            self.list.tail = None;
                        }
                    }

                    // current node gets disposed
                    Box::from_raw(self.ptr);
                }

                (value, None)
            }
        }
    }

    /// Moves the index to the right the specified amount of nodes. O(n) time.
    /// If the amount excedes the number of items returns None.
    pub fn forward(mut self, step: usize) -> Option<ListIndex<'a>> {
        for _i in 0..step {
            match unsafe { (*self.ptr).next } {
                Some(next_node) => {
                    self.index += 1;
                    self.ptr = next_node;
                },
                None =>  {
                    return None;
                },
            }
        }

        Some(self)
    }

    /// Moves the index to the left the specified amount of nodes. O(n) time.
    /// If the amount excedes the number of items returns None.
    pub fn backward(mut self, step: usize) -> Option<ListIndex<'a>> {
        for _i in 0..step {
            match unsafe { (*self.ptr).prev } {
                Some(prev_node) => {
                    self.index -= 1;
                    self.ptr = prev_node;
                }
                None => {
                    return None;
                },
            }
        }

        Some(self)
    }
}

#[cfg(test)]
mod tests {
    use super::List;

    #[test]
    fn push_front() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);
        list.push_front(1);
        assert_eq!(list.len(), 1);
        list.push_front(2);
        assert_eq!(list.len(), 2);
        list.push_front(3);
        assert_eq!(list.len(), 3);
    }

    #[test]
    fn push_back() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);
        list.push_back(1);
        assert_eq!(list.len(), 1);
        list.push_back(2);
        assert_eq!(list.len(), 2);
        list.push_back(3);
        assert_eq!(list.len(), 3);
    }

    #[test]
    fn pop_front() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);

        list.push_front(1);
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(1));
        assert_eq!(list.len(), 0);

        list.push_back(2);
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn pop_back() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);

        list.push_front(1);
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.len(), 0);

        list.push_back(2);
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_back(), Some(2));
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn as_a_queue() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);

        list.push_front(3);
        assert_eq!(list.len(), 1);
        list.push_front(2);
        assert_eq!(list.len(), 2);
        list.push_front(1);
        assert_eq!(list.len(), 3);

        assert_eq!(list.pop_back(), Some(3));
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop_back(), Some(2));
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.len(), 0);

        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn as_a_stack_front() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);

        list.push_front(1);
        assert_eq!(list.len(), 1);
        list.push_front(2);
        assert_eq!(list.len(), 2);
        list.push_front(3);
        assert_eq!(list.len(), 3);

        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(1));
        assert_eq!(list.len(), 0);

        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn as_a_stack_back() {
        let mut list = List::new();

        assert_eq!(list.len(), 0);

        list.push_back(1);
        assert_eq!(list.len(), 1);
        list.push_back(2);
        assert_eq!(list.len(), 2);
        list.push_back(3);
        assert_eq!(list.len(), 3);

        assert_eq!(list.pop_back(), Some(3));
        assert_eq!(list.len(), 2);
        assert_eq!(list.pop_back(), Some(2));
        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.len(), 0);

        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_push_middle_pop_front() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(4);

        let mut index = list.index(1).unwrap();

        index.push(3);
        index.push(2);

        assert_eq!(list.len(), 4);

        assert_eq!(list.pop_front(), Some(1));
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(list.pop_front(), Some(4));
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn index_push_middle_pop_back() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(4);

        let mut index = list.index(1).unwrap();

        index.push(3);
        index.push(2);

        assert_eq!(list.len(), 4);

        assert_eq!(list.pop_back(), Some(4));
        assert_eq!(list.pop_back(), Some(3));
        assert_eq!(list.pop_back(), Some(2));
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_push_beginning_pop_front() {
        let mut list = List::new();

        list.push_back(3);
        list.push_back(4);

        let mut index = list.index(0).unwrap();

        index.push(2);
        index.push(1);

        assert_eq!(list.len(), 4);

        assert_eq!(list.pop_front(), Some(1));
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(list.pop_front(), Some(4));
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn index_push_beginning_pop_back() {
        let mut list = List::new();

        list.push_back(3);
        list.push_back(4);

        let mut index = list.index(0).unwrap();

        index.push(2);
        index.push(1);

        assert_eq!(list.len(), 4);

        assert_eq!(list.pop_back(), Some(4));
        assert_eq!(list.pop_back(), Some(3));
        assert_eq!(list.pop_back(), Some(2));
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_push_pop_replace() {
        let mut list = List::new();

        list.push_back(0);
        list.push_back(2);
        list.push_back(4);

        let mut index = list.index(1).unwrap();

        assert_eq!(index.pos(), 1);
        assert_eq!(index.value(), 2);

        index.push(1);

        assert_eq!(index.pos(), 1);
        assert_eq!(index.replace(20), 1);
        assert_eq!(index.pos(), 1);

        assert_eq!(list.len(), 4);

        assert_eq!(list.pop_front(), Some(0));
        assert_eq!(list.pop_front(), Some(20));
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(list.pop_front(), Some(4));
    }

    #[test]
    fn index_pop_beginning_test_front() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(2);

        let index = list.index(0).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 1);

        let index = index.unwrap();

        assert_eq!(index.pos(), 0);
        assert_eq!(index.value(), 2);

        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(2));
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn index_pop_beginning_test_back() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(2);

        let index = list.index(0).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 1);

        let index = index.unwrap();

        assert_eq!(index.pos(), 0);
        assert_eq!(index.value(), 2);

        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_back(), Some(2));
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_pop_middle_test_front() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(2);
        list.push_back(3);

        let index = list.index(1).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 2);

        let index = index.unwrap();

        assert_eq!(index.pos(), 1);
        assert_eq!(index.value(), 3);

        assert_eq!(list.len(), 2);
        assert_eq!(list.pop_front(), Some(1));
        assert_eq!(list.pop_front(), Some(3));
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn index_pop_middle_test_back() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(2);
        list.push_back(3);

        let index = list.index(1).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 2);

        let index = index.unwrap();

        assert_eq!(index.pos(), 1);
        assert_eq!(index.value(), 3);

        assert_eq!(list.len(), 2);
        assert_eq!(list.pop_back(), Some(3));
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_pop_end_test_front() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(2);

        let index = list.index(1).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 2);

        assert!(index.is_none());

        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_front(), Some(1));
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn index_pop_end_test_back() {
        let mut list = List::new();

        list.push_back(1);
        list.push_back(2);

        let index = list.index(1).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 2);

        assert!(index.is_none());

        assert_eq!(list.len(), 1);
        assert_eq!(list.pop_back(), Some(1));
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_pop_only_element_test_front() {
        let mut list = List::new();

        list.push_back(1);

        let index = list.index(0).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 1);

        assert!(index.is_none());

        assert_eq!(list.len(), 0);
        assert_eq!(list.pop_front(), None);
    }

    #[test]
    fn index_pop_only_element_test_back() {
        let mut list = List::new();

        list.push_back(1);

        let index = list.index(0).unwrap();

        let (old_val, index) = index.pop();

        assert_eq!(old_val, 1);

        assert!(index.is_none());

        assert_eq!(list.len(), 0);
        assert_eq!(list.pop_back(), None);
    }

    #[test]
    fn index_move() {
        let mut list = List::new();

        list.push_back(0);
        list.push_back(2);
        list.push_back(4);

        let mut index = list.index(0).unwrap();

        assert_eq!(index.pos(), 0);
        assert_eq!(index.value(), 0);

        index = index.forward(1).unwrap();
        assert_eq!(index.pos(), 1);
        assert_eq!(index.value(), 2);

        index = index.forward(1).unwrap();
        assert_eq!(index.pos(), 2);
        assert_eq!(index.value(), 4);

        index = index.backward(1).unwrap();
        assert_eq!(index.pos(), 1);
        assert_eq!(index.value(), 2);

        index = index.backward(1).unwrap();
        assert_eq!(index.pos(), 0);
        assert_eq!(index.value(), 0);

        assert!(index.backward(1).is_none());
    }
}
